Description:
    This program creates a png file (picture) of a shp file (2D map). 

How to use:
    1. Move in your terminal until you get to the working directory 
    "Letter Creator".

    2. Run:
        -"python shp2png.py -B beforeMap.shp -A afterMap.shp" to get 
        output.png of the original map beforeMap.shp and its regionalization
        afterMap.shp in the same directory.

        -"python shp2png.py -B beforeMap.shp -A afterMap.shp -o outputName"
        to get outputName.png.

        NOTE: you can combine give a path for input or output

Tested with:
    -Mac OS X 10.8.2 (Mountain Lion)
    -Python 2.7.2
    -Clusterpy 0.9.9
    -Matplotlib 1.2.1
    -Descartes 1.0
    -Computer encoding UTF-8

Created by:
    Jose Builes Villegas (RiSE-group)
    web: www.rise-group.org
    email: contacto@rise-group.org
