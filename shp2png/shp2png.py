import clusterpy
import matplotlib.pyplot as plt
from descartes import PolygonPatch
import sys

def shp2png(beforePath, afterPath, outputName):
    
    # Create a figure plot
    fig = plt.figure(1)
    ax = fig.add_subplot(111)

    #Removes the frame from plot
    ax.axis("off")

    #This is the layer of the original map
    originalLayer = clusterpy.importArcData(beforePath) 
    #This is the layer of the map after the regionalization
    layer = clusterpy.importArcData(afterPath) 

    for area in originalLayer.areas:
        for ring in area:
            points=[]
            for point in ring:
                geomPntX,geomPntY = point
                points.append([geomPntX,geomPntY])
            
            # This is a geoJSONflike object
            polygon = {"type": "Polygon","coordinates":[points]}
            patch = PolygonPatch(polygon, fc= '#ffffff', 
                        ec='#a6a6a6', linewidth=0.5)
            ax.add_patch(patch)


    for area in layer.areas:
        for ring in area:
            points=[]
            for point in ring:
                geomPntX,geomPntY = point
                points.append([geomPntX,geomPntY])
            
            # This is a geoJSONflike object
            polygon = {"type": "Polygon","coordinates":[points]}
            patch = PolygonPatch(polygon,ec='#ff0000',fill=False)
            ax.add_patch(patch)


    x1,y1,x2,y2 = layer.bbox

    # Add a title
    title = layer.name
    if title != "":
        plt.title(title)

        yRange = y2-y1
        # Set the spatial extent f you could derive this from the shp bounding box
        ax.set_xlim([x1,x2])
        ax.set_ylim([y1,y2+yRange*0.1])
    else:
        ax.set_xlim([x1,x2])
        ax.set_ylim([y1,y2])

    ax.set_aspect(1)

    # Either plot the map, or save it to a file.
    plt.savefig(outputName + '.png', transparent=True)

if __name__ == "__main__":
    args = sys.argv[1:]

    if ("-B" in args):
        i = args.index("-B")
        beforePath = args[i+1]
        if len(beforePath) > 4:
            if beforePath[-4:]==".dbf" or beforePath[-4:]==".shp" or beforePath[-4:]==".shx":
                beforePath = beforePath[:-4]
    else:
        print "FAILED: You need to add an input shp, try '-B filePath'"
        sys.exit()


    if ("-A" in args):
        i = args.index("-A")
        afterPath = args[i+1]
        if len(afterPath) > 4:
            if afterPath[-4:]==".dbf" or afterPath[-4:]==".shp" or afterPath[-4:]==".shx":
                afterPath = afterPath[:-4]
    else:
        print "FAILED: You need to add an input shp, try '-A filePath'"
        sys.exit()

    if ("-o" in args):
        i = args.index("-o")
        outPath = args[i+1]
    else:
        outPath = "output"

    shp2png(beforePath, afterPath, outPath)
